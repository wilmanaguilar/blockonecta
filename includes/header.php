<?php 
    require_once '../clases/database.php'; 
	session_start();
?>
<!DOCTYPE html>
<html lang="es" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8">
		<title>Bienvenido a Bloc Konecta / modulo Admin</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<link href="../assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<link href="../assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		<link href="../assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="../assets/demo/demo9/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="#" />
	</head>
	<body  class="m--skin- m-page--loading-enabled m-page--loading m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default"  >
		<div class="m-page-loader m-page-loader--base">
			<div class="m-blockui">
				<span>
					Cargando...
				</span>
				<span>
					<div class="m-loader m-loader--brand"></div>
				</span>
			</div>
		</div>
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<header id="m_header" class="m-grid__item    m-header "  m-minimize="minimize" m-minimize-mobile="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200">
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop  m-header__wrapper">
						<div class="m-stack__item m-brand m-brand--mobile">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="admin.php" class="m-brand__logo-wrapper">
										<img alt="" src="../assets/ima/logos/logo.png"/>
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">
									<a href="javascript:;" id="m_aside_left_toggle_mobile" class="m-brand__icon m-brand__toggler m-brand__toggler--left">
										<span></span>
									</a>
									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler">
										<span></span>
									</a>
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon">
										<i class="flaticon-more"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="m-stack__item m-stack__item--middle m-stack__item--left m-header-head" id="m_header_nav">
							<div class="m-stack m-stack--ver m-stack--desktop">
								<div class="m-stack__item m-stack__item--middle m-stack__item--fit">
									<a href="javascript:;" id="m_aside_left_toggle" class="m-aside-left-toggler m-aside-left-toggler--left m_aside_left_toggler">
										<span></span>
									</a>
								</div>
								<div class="m-stack__item m-stack__item--fluid">
									<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
										<i class="la la-close"></i>
									</button>
								</div>
							</div>
						</div>
						<div class="m-stack__item m-stack__item--middle m-stack__item--center">
							<a href="admin.php" class="m-brand m-brand--desktop">
								<img alt="" src="../assets/ima/logos/logo.png"/>
							</a>
						</div>
						<div class="m-stack__item m-stack__item--right">
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">
										<li class="m-nav__item m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__username m--hidden-mobile">
													<?=$_SESSION['NombreCompleto'];?>
												</span>
												<span class="m-topbar__userpic">
													<img src="../assets/app/media/img/users/user3.jpg" class="m--img-rounded m--marginless m--img-centered" alt=""/>
												</span>
												<span class="m-nav__link-icon m-topbar__usericon  m--hide">
													<span class="m-nav__link-icon-wrapper">
														<i class="flaticon-user-ok"></i>
													</span>
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__header m--align-center">
														<div class="m-card-user m-card-user--skin-light">
															<div class="m-card-user__pic">
																<img src="../assets/app/media/img/users/user3.jpg" class="m--img-rounded m--marginless" alt=""/>
															</div>
															<div class="m-card-user__details">
																<span class="m-card-user__name m--font-weight-500">
																	<?=$_SESSION['NombreCompleto'];?>
																</span>
																<a href="" class="m-card-user__email m--font-weight-300 m-link">
																<?=$_SESSION['correo'];?>
																</a>
															</div>
														</div>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav m-nav--skin-light">
																<li class="m-nav__item">
																	<a href="../clases/acciones.php?accion=1" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
																		Cerrar sesión
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
				<i class="la la-close"></i>
			</button>
			<div id="m_aside_left" class="m-aside-left  m-aside-left--skin-dark ">
				<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" m-menu-scrollable="1" m-menu-dropdown-timeout="500">
					<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
						<li class="m-menu__section m-menu__section--first">
							<h4 class="m-menu__section-text">
								Menú
							</h4>
							<i class="m-menu__section-icon flaticon-more-v3"></i>
						</li><?php 
						if($_SESSION['tipousuario'] == '1'){?>
							<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
								<a  href="admin.php" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-suitcase"></i>
									<span class="m-menu__link-text">
										Inicio
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
								<a  href="../vistas/usuarios.php" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-users"></i>
									<span class="m-menu__link-text">
										Usuarios
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
								<a  href="../vistas/categorias.php" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-tabs"></i>
									<span class="m-menu__link-text">
										Categorias
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
								<a  href="../vistas/blocs.php" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-folder-1"></i>
									<span class="m-menu__link-text">
										Admin Bloc
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
								<a  href="articulos.php" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-folder-1"></i>
									<span class="m-menu__link-text">
										Bloc
									</span>
								</a>
							</li>
						<?php } else if($_SESSION['tipousuario'] == '2'){?>
							<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
								<a  href="articulos.php" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-list-1"></i>
									<span class="m-menu__link-text">
										Bloc
									</span>
								</a>
							</li>
						<?php } 
						?>
					</ul>
				</div>
			</div>