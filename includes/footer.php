            <!-- begin::Footer -->
            <footer class="m-grid__item  m-footer ">
				<div class="m-container m-container--responsive m-container--xxl m-container--full-height">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								2021 &copy; Desarrollado por 
								<a href="../assets/doc/HojaDeVida.pdf" target="_blank" class="m-link">
									Ingeniero Wilman Aguilar
								</a>
							</span>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>
		<script src="../assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>1
		<script src="../assets/demo/demo9/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="../assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
		<script src="../assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
		<script src="../assets/app/js/dashboard.js" type="text/javascript"></script>
        <script src="../assets/demo/default/custom/crud/datatables/basic/paginations.js" type="text/javascript"></script>
		<script>
            $(window).on('load', function() {
                $('body').removeClass('m-page--loading');         
            });
		</script>
	</body>
</html>
