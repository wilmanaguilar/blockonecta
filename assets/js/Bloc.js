
function validarFormularioNuevoBloc(){
    var idcategoria = document.getElementById("idcategoria").value;
    var titulo = document.getElementById("titulo").value;
    var txtcorto = document.getElementById("txtcorto").value;
    var txtlargo = document.getElementById("txtlargo").value;
    var estado = document.getElementById("estado").value;
    if(idcategoria == ""){
        alert("Error: La Categoria es obligatoria.");
        return false;
    } else if(titulo == ""){
        alert("Error: El Titulo es obligatorio.");
        return false;
    } else if(txtcorto == ""){
        alert("Error: La Descripción Breve es obligatoria.");
        return false;
    } else if(txtlargo == ""){
        alert("Error: La Descripción Completa es obligatoria.");
        return false;
    } else if(estado == ""){
        alert("Error: El Estado es obligatorio");
        return false;
    } else {
        return true;
    }
}
function validarFormularioEditarBloc(){
    var idcategoria = document.getElementById("idcategoria").value;
    var titulo = document.getElementById("titulo").value;
    var txtcorto = document.getElementById("txtcorto").value;
    var txtlargo = document.getElementById("txtlargo").value;
    var estado = document.getElementById("estado").value;
    if(idcategoria == ""){
        alert("Error: La Categoria es obligatoria.");
        return false;
    } else if(titulo == ""){
        alert("Error: El Titulo es obligatorio.");
        return false;
    } else if(txtcorto == ""){
        alert("Error: La Descripción Breve es obligatoria.");
        return false;
    } else if(txtlargo == ""){
        alert("Error: La Descripción Completa es obligatoria.");
        return false;
    } else if(estado == ""){
        alert("Error: El Estado es obligatorio");
        return false;
    } else {
        return true;
    }
}
