function validarFormularioLogin(){
    var correo = document.getElementById("correo").value;
    var pass = document.getElementById("pass").value;
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(correo) ){
        alert("Error: La Dirección de correo (" + correo + ") no es valida.");
        return false;
    } else if(pass == ""){
        alert("Error: La Contraseña es obligatoria.");
        return false;
    } else {
        return true;
    }
}