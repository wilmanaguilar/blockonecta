function validarFormularioRegistro(){
    var cedula = document.getElementById("cedula").value;
    var Nombre = document.getElementById("nombre").value;
    var correo = document.getElementById("correo").value;
    var clave = document.getElementById("clave").value;
    var claveCon = document.getElementById("claveCon").value;
    var noMovil = document.getElementById("noMovil").value;
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(cedula == ""){
        alert("Error: El Nombre es obligatorio.");
        return false;
    } else if(Nombre == ""){
        alert("Error: El Nombre es obligatorio.");
        return false;
    } else if ( !expr.test(correo) ){
        alert("Error: La Dirección de correo (" + correo + ") no es valida.");
        return false;
    } else if(clave == ""){
        alert("Error: La Contraseña es obligatoria.");
        return false;
    } else if(claveCon == ""){
        alert("Error: La Confirmacion de la Contraseña es obligatoria.");
        return false;
    } else if(claveCon != clave){
        alert("Error: Las contraseñas no coinciden");
        return false;
    } else if(noMovil == ""){
        alert("Error: El Número Móvil es obligatorio");
        return false;
    } else {
        return true;
    }
}
function validarFormularioNuevoUsuario(){
    var cedula = document.getElementById("cedula").value;
    var Nombre = document.getElementById("nombre").value;
    var correo = document.getElementById("correo").value;
    var clave = document.getElementById("clave").value;
    var claveCon = document.getElementById("claveCon").value;
    var noMovil = document.getElementById("noMovil").value;
    var tipo = document.getElementById("tipo").value;
    var estado = document.getElementById("estado").value;
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(cedula == ""){
        alert("Error: El Nombre es obligatorio.");
        return false;
    } else if(Nombre == ""){
        alert("Error: El Nombre es obligatorio.");
        return false;
    } else if ( !expr.test(correo) ){
        alert("Error: La Dirección de correo (" + correo + ") no es valida.");
        return false;
    } else if(clave == ""){
        alert("Error: La Contraseña es obligatoria.");
        return false;
    } else if(claveCon == ""){
        alert("Error: La Confirmacion de la Contraseña es obligatoria.");
        return false;
    } else if(claveCon != clave){
        alert("Error: Las contraseñas no coinciden");
        return false;
    } else if(noMovil == ""){
        alert("Error: El Número Móvil es obligatorio");
        return false;
    } else if(tipo == ""){
        alert("Error: El Tipo de Usuario es obligatorio");
        return false;
    } else if(estado == ""){
        alert("Error: El Estado es obligatorio");
        return false;
    } else {
        return true;
    }
}
function validarFormularioEditarUsuario(){
    var Nombre = document.getElementById("nombre").value;
    var correo = document.getElementById("correo").value;
    var noMovil = document.getElementById("noMovil").value;
    var tipo = document.getElementById("tipo").value;
    var estado = document.getElementById("estado").value;
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(Nombre == ""){
        alert("Error: El Nombre es obligatorio.");
        return false;
    } else if ( !expr.test(correo) ){
        alert("Error: La Dirección de correo (" + correo + ") no es valida.");
        return false;
    } else if(noMovil == ""){
        alert("Error: El Número Móvil es obligatorio");
        return false;
    } else if(tipo == ""){
        alert("Error: El Tipo de Usuario es obligatorio");
        return false;
    } else if(estado == ""){
        alert("Error: El Estado es obligatorio");
        return false;
    } else {
        return true;
    }
}
