function validarLetr(e)
{
    var key;
    if(window.event) // IE
    {
        key = e.keyCode;
    } else if(e.which) // Netscape/Firefox/Opera
    {
        key = e.which;
    }
    if ((key > 64 && key < 91) || (key > 96 && key < 123) || key == 08 || key == 32)
    {
        return true;
    }
    return false;
}
function validarNro(e) 
{
    var key;
    if(window.event) // IE
    {
        key = e.keyCode;
    }
    else if(e.which) // Netscape/Firefox/Opera
    {
        key = e.which;
    }
    if ((key > 45 && key < 58) || key == 08)
    {
        return true;
    }
    return false;
}
