<?php
    session_start();
    require_once('../includes/header.php');
    require_once '../clases/usuario.php';
    require_once('../clases/datos.php');
    $usuario = new usuario();
    if(!isset($_SESSION['id_usuario'])){
        print "<script>alert(\"Debe estar logueado\");window.location='../index.php';</script>";
    } else {
        if($_SESSION['tipousuario']=="2"){
            print "<script>alert(\"No tiene permiso para este modulo\");window.location='articulos.php';</script>";
        }
    }
?>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Usuarios
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="usuarios.php" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    Administracion de Usuarios
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Nuevo Usuario
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin: Datatable -->
                    <div class="contenidoFormulario">
                        <?php
                        require_once '../clases/database.php';
                        $db = database::conectar();
                    ?>
                    <form class="m-login__form m-form" method="post" action="../clases/acciones.php" onsubmit="return validarFormularioNuevoUsuario()">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Cédula</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="text" placeholder="Digite su Cedula" name="cedula" id="cedula" autocomplete="off" required onkeypress='javascript:return validarNro(event)'>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Nombre</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="text" placeholder="Digite su Nombre" name="nombre" id="nombre" autocomplete="off" required onkeypress='javascript:return validarLetr(event)'>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Correo Electronico</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="text" placeholder="Digite su Correo Electronico" name="correo" id="correo" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Contraseña</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="password" placeholder="Digita su Contraseña" name="clave" id="clave" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Confirmar Contraseña</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="password" placeholder="Digita confirmacion de su Contraseña" name="claveCon" id="claveCon" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Número Móvil</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="text" placeholder="Número Móvil" name="noMovil" id="noMovil" autocomplete="off" required onkeypress='javascript:return validarNro(event)'>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Tipo de Usuario</label>
                                <div class="col-10">
                                    <select name="tipo" id="tipo" class="form-control">
                                        <option value="">Seleccione Tipo de Usuario</option>
                                        <option value="1">Administrador</option>
                                        <option value="2">Usuario</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Estado de Usuario</label>
                                <div class="col-10">
                                    <select name="estado" id="estado" class="form-control">
                                        <option value="">Seleccione Estado</option>
                                        <option value="0">Inactivo</option>
                                        <option value="1">Activo</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="m-login__form-action" style="text-align: center;">
                            <input type="hidden" name="accion" value="nuevoUsuario">
                            <button value="Validar" id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
                                Registrar
                            </button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="../assets/js/Usuario.js"></script>
<?php
require_once '../includes/footer.php'; ?>