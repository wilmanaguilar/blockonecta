<!DOCTYPE html>
<html lang="en" >
	<head>
	<meta charset="utf-8"> 
		<title>Bloc konecta</title>		
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<link rel="stylesheet" type="text/css" href="../assets/css/styleIndex.css">
		<link href="../assets/css/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="../assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
	</head>
	<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url(../assets/ima/bg.jpg);">
				<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
								<img src="../assets/ima/logos/logo.png" width="320">
							</a>
						</div>
		  				<?php
							require_once '../clases/database.php';
		  					$db = database::conectar();
						?>
						<form class="m-login__form m-form" method="post" action="../clases/registroNuevo.php" onsubmit="return validarFormularioRegistro()">
							<div class="m-login__head">
								<h2 class="m-login__title"> Registro Nuevo Usuario</h2>
							</div>
							<div class="m-login__desc">
								Diligencie la siguiente informacion para su registro:
							</div>
							<div class="form-group m-form__group">
								<input class="form-control m-input" type="text" placeholder="Digite su Cedula" name="cedula" id="cedula" autocomplete="off" required onkeypress='javascript:return validarNro(event)'>
							</div>
							<div class="form-group m-form__group">
								<input class="form-control m-input" type="text" placeholder="Digite su Nombre" name="nombre" id="nombre" autocomplete="off" required onkeypress='javascript:return validarLetr(event)'>
							</div>
							<div class="form-group m-form__group">
								<input class="form-control m-input" type="text" placeholder="Digite su Correo Electronico" name="correo" id="correo" autocomplete="off" required>
							</div>
							<div class="form-group m-form__group">
								<input class="form-control m-input" type="password" placeholder="Digita su Contraseña" name="clave" id="clave" autocomplete="off" required>
							</div>
							<div class="form-group m-form__group">
								<input class="form-control m-input" type="password" placeholder="Digita confirmacion de su Contraseña" name="claveCon" id="claveCon" autocomplete="off" required>
							</div>
							<div class="form-group m-form__group">
								<input class="form-control m-input" type="text" placeholder="Número Móvil" name="noMovil" id="noMovil" autocomplete="off" required onkeypress='javascript:return validarNro(event)'>
							</div>		
							<div class="m-login__form-action">
								<button value="Validar" id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
									Registrar
								</button>
							</div>
							<div class="m-login__form-action">			
								<a href="../index.php">Volver</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="../assets/js/script.js"></script>
		<script type="text/javascript" src="../assets/js/Usuario.js"></script>
	</body>
</html>
