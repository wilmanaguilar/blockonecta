<?php
    session_start();
    require_once('../includes/header.php');
    require_once '../clases/categoria.php';
    require_once('../clases/datos.php');
    $categoria = new categoria();
    if(!isset($_SESSION['id_usuario'])){
        print "<script>alert(\"Debe estar logueado\");window.location='../index.php';</script>";
    } else {
        if($_SESSION['tipousuario']=="2"){
            print "<script>alert(\"No tiene permiso para este modulo\");window.location='articulos.php';</script>";
        }
    }
?>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Categorias
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    Administracion de Categorias
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Listado de Categorias
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="NuevaCategoria.php" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                                    <span class="kt-header__topbar-icon" id="kt_quick_panel_toggler_btn">
                                        <i class="la la-plus"></i>
                                        <span>
                                            Nueva Categoria
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-portlet__nav-item"></li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Categoria</th>			
                                <th>Estado</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                            <?php 
                            $query = $categoria->consultar();
                            while ($row = $query->fetch(PDO::FETCH_ASSOC)): ?>
                            <tr>
                                <td><?php echo $row['id']; ?></td>
                                <td><?php echo $row['nombre']; ?></td>
                                <td><?php 
                                    if($row['estado'] == '1'){
                                        echo "Activo";
                                    } else if($row['estado'] == '0'){
                                        echo "Inactivo";
                                    }
                                ?></td>
                                <td><a href="editarCategoria.php?id=<?php echo $row['id']; ?>">Modificar</a></td> 
                                <td>
                                    <?php 
                                        if($row['estado'] == '1'){ ?>
                                            <a href="../clases/acciones.php?accion=cambiarEstadoCategoria&id=<?php echo $row['id']; ?>&estado=0" onclick="return confirm('¿Esta seguro de cambiar el estado de esta Categoria?')"> <?php echo "Inactivar"; ?> </a> <?php
                                        } else if($row['estado'] == '0'){?>
                                            <a href="../clases/acciones.php?accion=cambiarEstadoCategoria&id=<?php echo $row['id']; ?>&estado=1" onclick="return confirm('¿Esta seguro de cambiar el estado de esta Categoria?')"> <?php echo "Activar"; ?> </a> <?php
                                        }
                                    ?>
                                    </a>
                                </td>
                            </tr>
                            <?php endwhile; ?>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once '../includes/footer.php'; ?>