<?php
session_start();
require_once('../includes/header.php');
require_once('../clases/datos.php');
require_once '../clases/bloc.php';
$bloc = new bloc();
if(!isset($_SESSION['id_usuario'])){
    print "<script>alert(\"Debe estar logueado\");window.location='../index.php';</script>";
}
?>	
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver    m-container m-container--responsive m-container--xxl m-page__container">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-content">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="m-portlet m-portlet--full-height ">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <span class="m-portlet__head-icon">
                                            <i class="flaticon-profile-1"></i>
                                        </span>
                                        <h3 class="m-portlet__head-text">Articulos disponibles</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                            <div class="container">
                                <div class="row" style="text-align: center;"><?php 
                                    $consultarArticulos = $bloc->consultarArticulosDetalle($_GET['id']); 
                                    while ($articulosRow = $consultarArticulos->fetch(PDO::FETCH_ASSOC)): ?>
                                        <div class="col-12" style=" border: 1px solid; padding-bottom: 30px; padding-top: 25px; padding-left: 30px; padding-right: 30px; border-radius: 30px; margin-bottom: 20px;">
                                            <h3 style="text-align: center;"><?php echo $articulosRow['titulo'];?></h3><br>
                                            <img src="../assets/ima/blocs/<?php echo $articulosRow['imagen'];?>" alt="imagen" width="500" height="500" style="border: 1px solid; margin-left: 13px; margin-right: 30px;"><br>
                                            <p style="text-align: justify; color:#CCCCCC; margin-top: 20px;"><strong>Descripción Corta: </strong><?php echo $articulosRow['txtCorto'];?></p>
                                            <p style="text-align: justify;"><strong>Descripción Completa: </strong><?php echo $articulosRow['txtLargo'];?></p>
                                            <p><strong>Categoria: </strong><?php echo $articulosRow['nombreCat'];?></p>
                                            
                                            <a href="javascript:history.back()" style="background-color: #5867dd; color: #FFFFFF; padding-left: 25px; padding-right: 25px; padding-top: 6px; padding-bottom: 6px; border-radius: 10px; box-shadow: 2px 2px 4px 1px #000; text-decoration: none;">VOLVER</a>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once('../includes/footer.php'); 
?>			