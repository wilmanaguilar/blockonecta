<?php
    session_start();
    require_once('../includes/header.php');
    require_once '../clases/categoria.php';
    require_once('../clases/datos.php');
    $categoria = new categoria();
    if(!isset($_SESSION['id_usuario'])){
        print "<script>alert(\"Debe estar logueado\");window.location='../index.php';</script>";
    } else {
        if($_SESSION['tipousuario']=="2"){
            print "<script>alert(\"No tiene permiso para este modulo\");window.location='articulos.php';</script>";
        }
    }
?>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Categorias
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="categorias.php" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    Administracion de Categorias
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Nueva Categoria
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin: Datatable -->
                    <div class="contenidoFormulario">
                        <?php
                        require_once '../clases/database.php';
                        $db = database::conectar();
                    ?>
                    <form class="m-login__form m-form" method="post" action="../clases/acciones.php" onsubmit="return validarFormularioNuevaCategoria()">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Nombre</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="text" placeholder="Nombre" name="nombre" id="nombre" autocomplete="off" required onkeypress='javascript:return validarLetr(event)'>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Estado de Categoria</label>
                                <div class="col-10">
                                    <select name="estado" id="estado" class="form-control">
                                        <option value="">Seleccione Estado</option>
                                        <option value="0">Inactivo</option>
                                        <option value="1">Activo</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="m-login__form-action" style="text-align: center;">
                            <input type="hidden" name="accion" value="nuevaCategoria">
                            <button value="Validar" id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
                                Registrar
                            </button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="../assets/js/Categoria.js"></script>
<?php
require_once '../includes/footer.php'; ?>