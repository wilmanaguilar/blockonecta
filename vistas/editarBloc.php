<?php
    session_start();
    require_once '../includes/header.php';
    require_once '../clases/categoria.php';
    require_once '../clases/datos.php';
    require_once '../clases/database.php';
    require_once '../clases/bloc.php';
    $categoria = new categoria();
    $bloc = new bloc();
    if(!isset($_SESSION['id_usuario'])){
        print "<script>alert(\"Debe estar logueado\");window.location='../index.php';</script>";
    } else {
        if($_SESSION['tipousuario']=="2"){
            print "<script>alert(\"No tiene permiso para este modulo\");window.location='articulos.php';</script>";
        }
    }
?>   
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Blocs
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="blocs.php" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    Administracion de Blocs
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Editar Blocs
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="contenidoFormulario">
                        <form class="kt-form kt-form--label-right" action="../clases/acciones.php" method="POST" onsubmit="return validarFormularioEditarBloc()" enctype="multipart/form-data">
							<div class="kt-portlet__body"><?php 
                                $consultarEditar = $bloc->consultarEditar($_GET['id']); 
                                while ($editarRow = $consultarEditar->fetch(PDO::FETCH_ASSOC)): ?>
                                    <div class="form-group row">
                                    <label for="example-tel-input" class="col-2 col-form-label">Categoria: </label>
                                    <div class="col-10">
                                        <select class="form-control" id="idcategoria" name="idcategoria" required="required">
                                            <option value="">Seleccione...</option><?php
                                                $consultarCategorias = $categoria->consultarCategorias();
                                                while ($row = $consultarCategorias->fetch(PDO::FETCH_ASSOC)){
                                                    if($editarRow['idcategoria'] == $row['id']) { ?>
                                                        <option selected="selected" value="<?php echo $row['id']; ?>"><?php echo $row['nombre']; ?></option><?php 
                                                    } else { ?>
                                                        <option value="<?php echo $row['id']; ?>"><?php echo $row['nombre']; ?></option>
                                                    <?php } 
                                                } ?>
                                        </select>
                                        <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $editarRow['id']?>" required="required">
                                        <input class="form-control" type="hidden" id="imganterior" name="imganterior" value="<?php echo $editarRow['imagen']?>" required="required">
                                    </div>
                                </div>
                                <?php
                                    require_once '../clases/database.php';
                                    $db = database::conectar();
                                ?>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Titulo</label>
                                    <div class="col-10">
                                        <input class="form-control m-input" value="<?php echo $editarRow['titulo'];?>" type="text" placeholder="Digite el titulo" name="titulo" id="titulo" autocomplete="off" required onkeypress='javascript:return validarLetr(event)'>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Descripción Breve</label>
                                    <div class="col-10">
                                        <textarea name="txtcorto" id="txtcorto" class="form-control" maxlength="100" required="required"><?php echo $editarRow['txtCorto'];?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Descripción Completa</label>
                                    <div class="col-10">
                                        <textarea name="txtlargo" id="txtlargo" class="form-control" minlength="100" maxlength="300" required="required"><?php echo $editarRow['txtLargo'];?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Imagen (La imagen debe ser cuadrada)</label>
                                    <img src="../assets/ima/blocs/<?php echo $editarRow['imagen'];?>" alt="imagen" width="50" height="50" style="border: 1px solid; margin-left: 13px; margin-right: 30px;">
                                    <div class="col-7">
                                        <input type="file" class="file" name="file" id="file" class="form-control" accept="image/*"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Estado de Usuario</label>
                                    <div class="col-10">
                                        <select name="estado" id="estado" class="form-control" required="required"><?php
                                                if($editarRow['estado'] == "0"){?>
                                                    <option value="">Seleccione Estado</option>
                                                    <option value="0" selected="selected">Inactivo</option>
                                                    <option value="1">Activo</option>
                                                <?php } else {?>
                                                    <option value="">Seleccione Estado</option>
                                                    <option value="0">Inactivo</option>
                                                    <option value="1" selected="selected">Activo</option>
                                                <?php }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="m-login__form-action" style="text-align: center;">
                                    <input type="hidden" name="accion" value="editarBloc">
                                    <button value="Validar" id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
                                        Editars
                                    </button>
                                </div>
                                <?php endwhile; ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="../assets/js/Bloc.js"></script>
<?php
require_once '../includes/footer.php'; ?>