<?php
session_start();
require_once('../includes/header.php');
require_once('../clases/datos.php');
if(!isset($_SESSION['id_usuario'])){
    print "<script>alert(\"Debe estar logueado\");window.location='../index.php';</script>";
} else {
    if($_SESSION['tipousuario']=="2"){
        print "<script>alert(\"No tiene permiso para este modulo\");window.location='articulos.php';</script>";
    }
}
?>	
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver    m-container m-container--responsive m-container--xxl m-page__container">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-content">
                <!--Begin::Section-->
                <div class="row">
                    <div class="col-xl-12">
                        <!--begin:: Widgets/Application Sales-->
                        <div class="m-portlet m-portlet--full-height ">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <span class="m-portlet__head-icon">
                                            <i class="flaticon-profile-1"></i>
                                        </span>
                                        <h3 class="m-portlet__head-text">Bienvenido(a) <?=$_SESSION['NombreCompleto'];?></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <p>Bienvenido a la aplicación web de Block Konecta Administración</p>
                            </div>
                        </div>
                        <!--end:: Widgets/Application Sales-->
                    </div>
                </div>
                <!--End::Section-->
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<?php
require_once('../includes/footer.php'); 
?>			