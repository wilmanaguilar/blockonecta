<?php
	class usuario {
		private $pdo;

		public function conexion(){
			require_once 'database.php';
			$database = new database();
			$this->pdo = $database->conectar();
		}
		public function consultar(){
			$this->conexion();
			$sql = "SELECT id, cedula, nombre, email, nomovil, tipousuario, fechaCreacion, estado            
				FROM usuarios";
			try {
				$consulta = $this->pdo->query($sql);
				return $consulta;
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function insertarUsuario($cedula, $nombre, $correo, $clave, $noMovil, $tipo, $estado){
			$this->conexion();
			$sql_consulta = "SELECT count(1) as total FROM usuarios where cedula = '".$cedula."'";
			$consulta = $this->pdo->query($sql_consulta);
			
			while ($row = $consulta->fetch(PDO::FETCH_ASSOC)): 
				$total = $row['total'];
			endwhile;
			if($total == '0'){
				$sql="INSERT INTO usuarios (cedula, nombre, email, clave, nomovil, tipousuario, fechaCreacion, estado) VALUES ('".$cedula."', '".$nombre."', '".$correo."', '".md5($clave)."', '".$noMovil."', '".$tipo."', '".date('Y-m-d')."', '".$estado."')";
				try {
					$this->pdo->query($sql);
					return "1";
				} catch (Exception $e) {
					return $e->getMessage();
				}
			} else {
				return "0";
			}
		}
		public function cambiarEstadoUsuario($id,$estado){
            $this->conexion();
			$sql = "UPDATE usuarios SET estado = ".$estado." WHERE id = '".$id."'";
			$this->pdo->query($sql);
			return "1"; 
        }
		public function consultarEditar($id){
			$this->conexion();
			$sql = "SELECT id, cedula, nombre, email, nomovil, tipousuario, fechaCreacion, estado FROM usuarios where id = '".$id."'";
			try {
				$consulta = $this->pdo->query($sql);
				return $consulta;
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function editarUsuario($id, $nombre, $correo, $noMovil, $tipo, $estado){
			$this->conexion();
			$sql = "UPDATE usuarios SET nombre = '".$nombre."', email = '".$correo."', nomovil = '".$noMovil."', tipousuario = ".$tipo.", estado = '".$estado."'
							WHERE id = '".$id."'";
			$this->pdo->query($sql);
			return "1";
		}
	}
?>
