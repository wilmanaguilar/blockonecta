<?php
	class bloc {
		private $pdo;

		public function conexion(){
			require_once 'database.php';
			$database = new database();
			$this->pdo = $database->conectar();
		}
		public function consultar(){
			$this->conexion();
			$sql = "SELECT blo.id, cat.nombre as nombreCat, blo.titulo, blo.txtCorto, blo.fechaCreacion, blo.estado
				      FROM blocs as blo
                inner join categorias as cat on cat.id = blo.idcategoria";
			try {
				$consulta = $this->pdo->query($sql);
				return $consulta;
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function insertarBloc($idcategoria, $titulo, $txtcorto, $txtlargo, $estado, $nombre_img){
			$this->conexion();
            $sql="INSERT INTO blocs (idcategoria, titulo, txtCorto, txtLargo, imagen, fechaCreacion, estado) VALUES ('".$idcategoria."', '".$titulo."', '".$txtcorto."', '".$txtlargo."', '".$nombre_img."', '".date('Y-m-d')."', '".$estado."')";
            try {
                $this->pdo->query($sql);
                return "1";
            } catch (Exception $e) {
                return $e->getMessage();
            }
		}
		public function cambiarEstadoBloc($id,$estado){
            $this->conexion();
			$sql = "UPDATE blocs SET estado = ".$estado." WHERE id = '".$id."'";
			$this->pdo->query($sql);
			return "1"; 
        }
		public function consultarEditar($id){
			$this->conexion();
			$sql = "SELECT id, idcategoria, titulo, txtCorto, txtLargo, imagen, estado FROM blocs where id = '".$id."'";
			try {
				$consulta = $this->pdo->query($sql);
				return $consulta;
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function editarBloc($idcategoria, $titulo, $txtcorto, $txtlargo, $estado, $nombre_img, $id){
			$this->conexion();
            if($nombre_img == ""){
                $sql = "UPDATE blocs SET idcategoria = '".$idcategoria."', titulo = '".$titulo."', txtCorto = '".$txtcorto."', txtLargo = '".$txtlargo."', estado = '".$estado."'
                            WHERE id = '".$id."'";
            } else {
                $sql = "UPDATE blocs SET idcategoria = '".$idcategoria."', titulo = '".$titulo."', txtCorto = '".$txtcorto."', txtLargo = '".$txtlargo."', imagen = '".$nombre_img."', estado = '".$estado."'
                            WHERE id = '".$id."'";
            }
			$this->pdo->query($sql);
			return "1";
		}
		public function consultarArticulos(){
			$this->conexion();
			$sql = "SELECT blo.id as idbloc, cat.nombre as nombreCat, blo.imagen, blo.titulo, blo.txtCorto, blo.fechaCreacion, blo.estado
				      FROM blocs as blo
                inner join categorias as cat on cat.id = blo.idcategoria 
				     where cat.estado = '1' and blo.estado = '1'";
			try {
				$consulta = $this->pdo->query($sql);
				return $consulta;
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function consultarArticulosDetalle($id){
			$this->conexion();
			$sql = "SELECT blo.id, cat.nombre as nombreCat, blo.imagen, blo.titulo, blo.txtCorto, blo.txtLargo, blo.fechaCreacion, blo.estado
				      FROM blocs as blo
                inner join categorias as cat on cat.id = blo.idcategoria 
				     where cat.estado = '1' and blo.estado = '1' 
				       and blo.id = '".$id."'";
			try {
				$consulta = $this->pdo->query($sql);
				return $consulta;
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
	}
?>
