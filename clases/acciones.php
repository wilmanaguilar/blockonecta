<?php 
require_once 'login.php';
require_once 'logout.php';
require_once 'usuario.php';
require_once 'categoria.php';
require_once 'bloc.php';
$metodo = $_GET['accion'];
if (isset($metodo)) {
    if($metodo == '1'){
        $logout = new logout();
        $logout->Logout_user();
    } else if($metodo == "cambiarEstadoUsuario") {
        $id = $_GET['id'];
        $estado = $_GET['estado'];
        $usuario = new usuario();
        $usuario->cambiarEstadoUsuario($id,$estado);
        print "<script>alert(\"Cambio de estado exitoso. \");window.location='../vistas/usuarios.php';</script>";
    } else if($metodo == "cambiarEstadoCategoria") {
        $id = $_GET['id'];
        $estado = $_GET['estado'];
        $categoria = new categoria();
        $categoria->cambiarEstadoCategoria($id,$estado);
        print "<script>alert(\"Cambio de estado exitoso. \");window.location='../vistas/categorias.php';</script>";
    } else if($metodo == "cambiarEstadoBloc") {
        $id = $_GET['id'];
        $estado = $_GET['estado'];
        $bloc = new bloc();
        $bloc->cambiarEstadoBloc($id,$estado);
        print "<script>alert(\"Cambio de estado exitoso. \");window.location='../vistas/blocs.php';</script>";
    }
} else {
    $metodo = $_POST['accion'];
    if($metodo == '1'){
        $Login = new Login();
        $Login->Login_user($_POST['correo'], $_POST['pass']);
    } else if($metodo == "nuevoUsuario"){
        $cedula = $_POST["cedula"];
        $nombre = $_POST["nombre"];
        $correo = $_POST["correo"];
        $clave = $_POST["clave"];
        $noMovil = $_POST["noMovil"];
        $tipo = $_POST["tipo"];
        $estado = $_POST["estado"];

        $usuario = new usuario();
        $salida = $usuario->insertarUsuario($cedula, $nombre, $correo, $clave, $noMovil, $tipo, $estado);
        if($salida == '0'){
            print "<script>alert(\"La Cédula ya se encuentra en nuestro sistema verifiquela he intentelo de nuevo. \");window.location='../vistas/NuevoUsuario.php';</script>";
        } else {
            print "<script>alert(\"Usuario agregado exitosamente. \");window.location='../vistas/usuarios.php';</script>";
        }   
    } else if($metodo == "editarUsuario"){
        $nombre = $_POST["nombre"];
        $correo = $_POST["correo"];
        $noMovil = $_POST["noMovil"];
        $tipo = $_POST["tipo"];
        $estado = $_POST["estado"];
        $id = $_POST["id"];
        $usuario = new usuario();
        $usuario->editarUsuario($id, $nombre, $correo, $noMovil, $tipo, $estado);
        print "<script>alert(\"Usuario actualizado exitosamente. \");window.location='../vistas/usuarios.php';</script>";
    } else if($metodo == "nuevaCategoria"){
        $nombre = $_POST["nombre"];
        $estado = $_POST["estado"];

        $categoria = new categoria();
        $salida = $categoria->insertarCategoria($nombre, $estado);
        print "<script>alert(\"Categora agregada exitosamente. \");window.location='../vistas/categorias.php';</script>";
    } else if($metodo == "editarCategoria"){
        $nombre = $_POST["nombre"];
        $estado = $_POST["estado"];
        $id = $_POST["id"];
        $categoria = new categoria();
        $categoria->editarCategoria($id, $nombre, $estado);
        print "<script>alert(\"Categoria actualizada exitosamente. \");window.location='../vistas/categorias.php';</script>";
    } else if($metodo == "nuevoBloc"){
        $idcategoria = $_POST["idcategoria"];
        $titulo = $_POST["titulo"];
        $txtcorto = $_POST["txtcorto"];
        $txtlargo = $_POST["txtlargo"];
        $estado = $_POST["estado"];
        if (isset ($_FILES["file"])) {
            $archivos = "";
            $tmp_name = $_FILES["file"]["tmp_name"];
            $name = $_FILES["file"]["name"];
            $status = "";
            $prefijo = substr(md5(uniqid(rand())),0,6);
            $nombre_img = $prefijo."_".$name;
            if ($name != "") 
            {
                $destino =  "../assets/ima/blocs/".$nombre_img;
                if (copy($tmp_name,$destino)){
                    $bloc = new bloc();
                    $salida = $bloc->insertarBloc($idcategoria, $titulo, $txtcorto, $txtlargo, $estado, $nombre_img);
                    print "<script>alert(\"Bloc agregado exitosamente. \");window.location='../vistas/blocs.php';</script>";
                }
            }
        }
    } else if($metodo == "editarBloc"){
        echo "1-";
        $idcategoria = $_POST["idcategoria"];
        $titulo = $_POST["titulo"];
        $txtcorto = $_POST["txtcorto"];
        $txtlargo = $_POST["txtlargo"];
        $estado = $_POST["estado"];
        $id = $_POST["id"];
        $imganterior = $_POST["imganterior"];
        $nombre_img = "";

        if ($_FILES["file"]["name"]!="") {
            unlink("../assets/ima/blocs/".$imganterior);
            $archivos = "";
            $tmp_name = $_FILES["file"]["tmp_name"];
            $name = $_FILES["file"]["name"];
            $status = "";
            $prefijo = substr(md5(uniqid(rand())),0,6);
            $nombre_img = $prefijo."_".$name;
            if ($name != "") 
            {
                $destino =  "../assets/ima/blocs/".$nombre_img;
                if (copy($tmp_name,$destino)){
                    $bloc = new bloc();
                    $salida = $bloc->editarBloc($idcategoria, $titulo, $txtcorto, $txtlargo, $estado, $nombre_img, $id);
                    print "<script>alert(\"Bloc actualizada exitosamente. \");window.location='../vistas/blocs.php';</script>";
                }
            }
        } else {
            $bloc = new bloc();
            $salida = $bloc->editarBloc($idcategoria, $titulo, $txtcorto, $txtlargo, $estado, $nombre_img, $id);
            print "<script>alert(\"Bloc actualizada exitosamente. \");window.location='../vistas/blocs.php';</script>";
        }
    }
}
?>