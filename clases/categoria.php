<?php
	class categoria {
		private $pdo;

		public function conexion(){
			require_once 'database.php';
			$database = new database();
			$this->pdo = $database->conectar();
		}
		public function consultar(){
			$this->conexion();
			$sql = "SELECT id, nombre, estado
				FROM categorias";
			try {
				$consulta = $this->pdo->query($sql);
				return $consulta;
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function insertarCategoria($nombre, $estado){
			$this->conexion();
			$sql="INSERT INTO categorias (nombre, estado) VALUES ('".$nombre."', '".$estado."')";
			try {
				$this->pdo->query($sql);
				return "1";
			} catch (Exception $e) {
				return $e->getMessage();
			}
		}
		public function cambiarEstadoCategoria($id,$estado){
            $this->conexion();
			$sql = "UPDATE categorias SET estado = ".$estado." WHERE id = '".$id."'";
			$this->pdo->query($sql);
			return "1"; 
        }
		public function consultarEditar($id){
			$this->conexion();
			$sql = "SELECT id, nombre, estado FROM categorias where id = '".$id."'";
			try {
				$consulta = $this->pdo->query($sql);
				return $consulta;
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function editarCategoria($id, $nombre, $estado){
			$this->conexion();
			$sql = "UPDATE categorias SET nombre = '".$nombre."', estado = '".$estado."'
							WHERE id = '".$id."'";
			$this->pdo->query($sql);
			return "1";
		}
		public function consultarCategorias(){
			$this->conexion();
			$sql = "SELECT id, nombre FROM categorias where estado = '1' order by nombre";
			try {
				$consulta = $this->pdo->query($sql);
				return $consulta;
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
	}
?>
