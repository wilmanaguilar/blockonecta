<?php
    class database {
        private static $dbHost = "127.0.0.1";
        private static $dbName = "blockonecta";
        private static $dbUsername = "root";
        private static $dbUserpassword = "";

        public static function conectar(){
            try {
                $pdo = new PDO("mysql:host=".self::$dbHost.";dbname=".self::$dbName,self::$dbUsername,self::$dbUserpassword);
                $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                return $pdo;
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }
?>