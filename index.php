<!DOCTYPE html>
<html lang="en" >
<?php 
	session_start();
	if(isset($_SESSION['id_usuario'])){
		if($_SESSION['tipousuario']=="2"){
            print "<script>alert(\"Bienvenido!\");window.location='vistas/articulos.php';</script>";
        } else {
            print "<script>alert(\"Bienvenido!\");window.location='vistas/admin.php';</script>";
        }
		
	}
?>
	<head>
	<meta charset="utf-8"> 
		<title>Bloc konecta</title>
				
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<link rel="stylesheet" type="text/css" href="assets/css/styleIndex.css">
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
	</head>
	<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url(assets/ima/bg.jpg);">
				<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__head">
							<a href="#">
								<img src="assets/ima/logos/logo.png" width="320">
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Ingresa tus credenciales
								</h3>
							</div>
							<form class="m-login__form m-form" method="post" action="clases/acciones.php" onsubmit="return validarFormularioLogin()">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Correo electronico" name="correo" id="correo" autocomplete="off" required="">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Contraseña" name="pass" id="pass" required>
								</div>
								<div class="row m-login__form-sub">
									<div class="col m--align-right m-login__form-right">
										<a href="vistas/registroUsuario.php" id="m_login_forget_password" class="m-link">
											¿No tienes cuenta? Regístrate
										</a>
									</div>
								</div>
								<div class="m-login__form-action">
									<input type="hidden" name="accion" value="1">
									<button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
										Iniciar Sesión
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="assets/js/login.js"></script>
	</body>
</html>
